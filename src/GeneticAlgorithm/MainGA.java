package GeneticAlgorithm;

import static GeneticAlgorithm.GeneticAlg.nodeNumber;
import static GeneticAlgorithm.Population.getFitnessNumberOfIndividual;
import static GeneticAlgorithm.Population.getMaxDegreeOfThePopulation;
import static GeneticAlgorithm.Population.numberOfPop;
import static GeneticAlgorithm.Population.pop;
import static GeneticAlgorithm.Population.tempPop;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MainGA {
	static int generationNumber = 100;

	public static void run() {
		for (int i = 0; i < generationNumber; i++) {
			selection(pop);
			crossover(pop);
			mutation(pop);

		}
		if (getFitnessNumberOfIndividual(pop[0]) >= 1000) {
			int maxDegreeOfThePopulation = getMaxDegreeOfThePopulation();
			for (int i = 0; i < pop.length; i++) {
				for (int j = 0; j < pop[0].length; j++) {
					pop[i][j] = (int) (Math.random() * maxDegreeOfThePopulation);
				}
			}
		}

		int[] adj2 = new int[pop[0].length];
		HashMap hm = new HashMap();
		hm.put(pop[0][0], 0);
		for (int i = 1; i < adj2.length; i++) {
			for (int j = 0; j < i; j++) {
				if (pop[0][i] != pop[0][j]) {
					hm.put(pop[0][i], 0);
				}
			}
		}

		Set set = hm.entrySet();
		Iterator a = set.iterator();
		int as = 0;
		// Display elements
		while (a.hasNext()) {
			Map.Entry me = (Map.Entry) a.next();
			me.setValue(as++);
		}

		for (int i = 0; i < adj2.length; i++) {
			adj2[i] = (int) hm.get(pop[0][i]);
		}

		int max1 = 0;
		for (int i = 0; i < adj2.length; i++) {
			if (adj2[i] > max1)
				max1 = adj2[i];
		}

		
			System.out.println("No of colors: " + (max1 + 1));
	}


	private static void selection(int[][] pop) {
		System.arraycopy(pop, 0, tempPop, 0, pop.length);
		Population.bestIndividual = tempPop[0];

	//	random_selection2(tempPop, ratio_rand, best_fitt);
		tempPop[0] = Population.bestIndividual.clone();

//		eiga_selection2(tempPop, ratio_eiga, best_fitt);
	//	tempPop[0] = Population.bestIndividual.clone();

//		System.arraycopy(tempPop, 0, pop, 0, pop.length);
		mainSelection(tempPop);
		System.arraycopy(tempPop, 0, pop, 0, pop.length);
	}

	public static void mainSelection(int[][] pop) {
		int firstIndividual;
		int secondIndividual;
		for (int i = 1; i < numberOfPop; i++) {
			firstIndividual = 1 + (int) (Math.random() * (numberOfPop - 1));
			while (true) {
				secondIndividual = 1 + (int) (Math.random() * (numberOfPop - 1));
				if (firstIndividual != secondIndividual)
					break;
			}
			if (getFitnessNumberOfIndividual(pop[firstIndividual]) < getFitnessNumberOfIndividual(pop[secondIndividual])) {
				tempPop[i] = pop[firstIndividual].clone();
			} else
				tempPop[i] = pop[secondIndividual].clone();
		}
	}


	/*crossover at half*/
	private static void crossover(int[][] pop) {
		int firstIndividual;
		int secondIndividual;
		for (int u = 1; u < pop.length; u++) {
			firstIndividual = 1 + (int) (Math.random() * (numberOfPop - 1));
			secondIndividual = 1 + (int) (Math.random() * (numberOfPop - 1));
			System.arraycopy(pop[firstIndividual], 0, tempPop[u], 0,
					pop[firstIndividual].length / 2);
			System.arraycopy(pop[secondIndividual],
					pop[secondIndividual].length / 2, tempPop[u],
					pop[secondIndividual].length / 2,
					pop[secondIndividual].length / 2);

		}
		System.arraycopy(tempPop, 0, pop, 0, pop.length);
	}

	//interchange between two random positions
	private static void mutation(int[][] pop) {
		double p;
		double pm = 0.2; // (double) 1/pop.length;

		int firstPos;
		int secPos;
		for (int i = 1; i < pop.length; i++) {
			p = Math.random();
			firstPos = (int) (Math.random() * (nodeNumber));
			secPos = (int) (Math.random() * (nodeNumber));
			if (p < pm) {
				int temp = pop[i][firstPos];
				pop[i][firstPos] = pop[i][secPos];
				pop[i][secPos] = temp;
			}

		}
	}
}
