package GeneticAlgorithm;

import static GeneticAlgorithm.GeneticAlg.nodeNumber;

import java.util.ArrayList;

public class Population {
	static int numberOfPop;
	static Nodes[] nodes;
	static int[] bestIndividual;
	static int[][] pop;
	static int fitness[];
	static int tempPop[][];

	public Population(int numberOfPop, int numberOfNodes) {
		Population.numberOfPop = numberOfPop;
		nodes = new Nodes[numberOfNodes];
		for (int i = 0; i < numberOfNodes; i++)
			nodes[i] = new Nodes();
		pop = new int[numberOfPop][numberOfNodes];
		bestIndividual = new int[numberOfNodes];

	}

	public static void initialization() {

		//max degree because that is the minimum number of different colors that we need to color the graph
		int max_degree = getMaxDegreeOfThePopulation();
		for (int i = 0; i < pop.length; i++) {
			for (int j = 0; j < pop[0].length; j++) {
				pop[i][j] = (int) (Math.random() * max_degree);
			}
		}

		bestIndividual = pop[0];
		tempPop = new int[numberOfPop][nodeNumber];
		fitness = new int[numberOfPop];
		for (int i = 0; i < numberOfPop; i++) {
			fitness[i] = getFitnessNumberOfIndividual(pop[i]);
		}
	}

	public static int getMaxDegreeOfThePopulation() {
		int max = 0;
		for (int i = 0; i < nodes.length; i++) {
			if (nodes[i].adjacents.size() > max) {
				max = nodes[i].adjacents.size();
			}
		}
		return max;
	}

	public static int getFitnessNumberOfIndividual(int[] individual) {
		int violation = 0;
		int totalColor = numberOfUniqueElements(individual);
		for (int i = 0; i < nodeNumber; i++) {
			for (int j = i + 1; j < nodeNumber; j++) {
				if ((nodes[i].adjacents.contains(j)) && (i != j)
						&& (individual[i] == individual[j]))
					violation++;
			}
		}
		if (violation == 0)
			return totalColor;
		else
			return totalColor * 1000;

	}

	public static int numberOfUniqueElements(int[] individual) {
		ArrayList<Integer> tempList = new ArrayList<Integer>();
		for (int i = 0; i < individual.length; i++) {
			if (!tempList.contains(individual[i]))
				tempList.add(individual[i]);
		}
		return tempList.size();
	}

}
