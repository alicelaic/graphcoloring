package GeneticAlgorithm;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class GeneticAlg {
	static int nodeNumber;
	static int edgeNumber;
	public static void read(File graphFile){

		try {
			Scanner scanner = new Scanner(graphFile);
		    nodeNumber = scanner.nextInt();
		    edgeNumber = scanner.nextInt();
		    Population population = new Population(20, nodeNumber);

		    for(int i = 0; i < edgeNumber; i++){
		    	int a = scanner.nextInt();
		    	int b = scanner.nextInt();
		    	population.nodes[a].adjacents.add(b);
		    	population.nodes[b].adjacents.add(a);
		    }		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}

}
