package Graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import GeneticAlgorithm.GeneticAlg;
import GeneticAlgorithm.Population;
import GeneticAlgorithm.MainGA;

public class Main  {

	static File graphFile;
	static int nodeNumber;
	static int edgeNumber;
	static int adj[][];

	public static void read_file_write_to_array() {

		try {
			Scanner scanner = new Scanner(graphFile);
			nodeNumber = scanner.nextInt();
			edgeNumber = scanner.nextInt();

			adj = new int[nodeNumber][nodeNumber];

			for (int i = 0; i < edgeNumber; i++) {
				int a = scanner.nextInt();
				int b = scanner.nextInt();
				adj[a][b] = 1;
				adj[b][a] = 1;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		graphFile =new File("gc_20_1.txt");
		GeneticAlg.read(graphFile);
		Population.initialization();
		MainGA.run();

	}
}
